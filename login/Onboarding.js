import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Colors, {
  Gray1,
  Gray2,
  Gray3,
  Gray4,
  Gray5,
  Black1,
  Black2,
  Black3,
  White,
  Info,
  Success,
  Warning,
  Error,
} from '../../utils/Colors';
import Icon from 'react-native-vector-icons/AntDesign';
import AppIntroSlider from 'react-native-app-intro-slider';
import {useNavigation} from '@react-navigation/core';

const slides = [
  {
    key: 1,
    title: 'Hi, Selamat Datang',
    text: 'Di Flexia kamu bisa pesan tiket\nPesawat, Kereta, Hotel, cari Event\nhingga sewa Mobil',
    image: require('../img/onboarding1.png'),
    backgroundColor: White,
  },
  {
    key: 2,
    title: 'Makin Mudah Bepergian',
    text: 'Dengan berbagai fitur yang ada buat kamu\ndi Flexia, liburan atau perjalananmu\nbakal makin mudah dan nyaman',
    image: require('../img/onboarding2.png'),
    backgroundColor: White,
  },
  {
    key: 3,
    title: 'Banyak Fitur Baru',
    text: 'Kini Flexia punya banyak fitur baru\ndan tampilan baru, membuat kamu\nmenjadi semakin nyaman',
    image: require('../img/onboarding3.png'),
    backgroundColor: White,
  },
];

export default class Onboarding extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false,
    };
  }

  renderSkip = () => {
    return (
      <View>
        <Text
          style={{
            color: Gray4,
            fontSize: 16,
            marginLeft: '4%',
            fontWeight: 'bold',
          }}>
          Lewati
        </Text>
      </View>
    );
  };

  renderNext = () => {
    return (
      <View>
        <Text
          style={{
            color: Info,
            fontSize: 16,
            marginRight: '4%',
            fontWeight: 'bold',
          }}>
          Lanjutkan
        </Text>
      </View>
    );
  };

  renderDone = () => {
    return (
      <View>
        <Text
          style={{
            color: Info,
            fontSize: 16,
            marginRight: '4%',
            fontWeight: 'bold',
          }}>
          Mulai
        </Text>
      </View>
    );
  };

  _renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <Image source={item.image} />
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  };
  _onDone = () => {
    this.props.navigation.replace('Login');
    this.setState({showRealApp: true});
  };
  render() {
    if (this.state.showRealApp) {
      return <Onboarding />;
    } else {
      return (
        <AppIntroSlider
          renderItem={this._renderItem}
          data={slides}
          onDone={this._onDone}
          showNextButton={true}
          renderNextButton={this.renderNext}
          showSkipButton={true}
          renderSkipButton={this.renderSkip}
          showDoneButton={true}
          renderDoneButton={this.renderDone}
          activeDotStyle={{backgroundColor: Gray3, marginBottom: 100}}
          dotClickEnabled={true}
          dotStyle={{backgroundColor: 'rgba(0, 0, 0, .9)', marginBottom: 100}}
        />
      );
    }
  }
}

const styles = StyleSheet.create({
  slide: {
    paddingTop: '15%',
    alignItems: 'center',
    backgroundColor: 'white',
    height: '100%',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 24,
    marginTop: 25,
    color: Gray1,
    marginHorizontal: '7%',
    marginBottom: 15,
  },
  text: {
    fontSize: 16,
    textAlign: 'center',
    color: Gray3,
  },
  image: {
    width: '100%',
    height: 350,
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
