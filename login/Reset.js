import React, { Component } from 'react'
import { Text, TextInput, View, Image, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import Colors, { Gray1, Gray2, Gray3, Gray4, Gray5, Black1, Black2, Black3, White, Info, Success, Warning, Error } from '../../utils/Colors'
import Icon from 'react-native-vector-icons/AntDesign'
import { useNavigation } from '@react-navigation/core'

export class Register extends Component {

    constructor() {
        super()
        this.state = {
            viewPassword: true,
        }
    }
    render() {
        const { navigation } = this.props;

        return (
            <ScrollView style={{ backgroundColor: White }}>
                <View style={{ height: 50, width: '100%', backgroundColor: Info, alignItems: 'center', paddingLeft: 10, flexDirection: 'row' }}>
                    <Icon name='left' size={22} style={{ color: White }} onPress={() => {
                        this.props.navigation.goBack()
                    }} />
                    <Text style={{ fontSize: 18, marginLeft: 20, color: White }}>Reset Password</Text>
                </View>
                <Text style={styles.judul}>
                    Atur ulang kata sandi
                </Text>
                <View style={styles.container}>
                    <Text style={[styles.text, { marginBottom: 15 }]}>
                        Masukan e-mail yang terdaftar. Kami akan mengirimkan kode verifikasi untuk atur ulang kata sandi.
                    </Text>
                    <View style={styles.kotak}>
                        <Icon name='user' size={22} style={styles.icon} />
                        <TextInput placeholder='Masukan Email kamu' style={styles.textPlaceholder} />
                    </View>
                    <TouchableOpacity style={styles.button}>
                        <Text style={{ color: White, fontSize: 16 }}>Lanjut</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView >
        )
    }
}

export default Register

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
        height: '100%',
        paddingHorizontal: '4%'
    },
    judul: {
        fontWeight: 'bold',
        fontSize: 24,
        marginTop: 25,
        color: Black2,
        marginHorizontal: '4%',
        marginTop: 25,
        marginBottom: 15,
    },
    text: {
        fontSize: 16,
        textAlign: 'left',
        color: Gray3,
    },
    kotak: {
        flexDirection: 'row',
        width: '100%',
        borderWidth: .5,
        height: 45,
        borderRadius: 5,
        borderColor: Gray5,
        backgroundColor: White,
        alignItems: 'center',
        elevation: 1,
        marginBottom: 10
    },
    icon: {
        marginHorizontal: 10,
        color: Gray3
    },
    icon2: {
        position: 'absolute',
        right: 0,
        marginRight: 10,
        zIndex: 1,
        color: Gray3
    },
    textPlaceholder: {
        color: Gray3,
        width: '100%'
    },
    button: {
        width: '100%',
        height: 45,
        borderRadius: 5,
        backgroundColor: Info,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10
    }
})