import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import Colors, {
  Gray1,
  Gray2,
  Gray3,
  Gray4,
  Gray5,
  Black1,
  Black2,
  Black3,
  White,
  Info,
  Success,
  Warning,
  Error,
} from '../../utils/Colors';
import Icon from 'react-native-vector-icons/AntDesign';
import {useNavigation} from '@react-navigation/core';

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewPassword: true,
      isLoading: true,
      dataSource: null,
    };
  }

  // componentDidMount() {
  //     return fetch('https://flexia-app.herokuapp.com/api/login-user')
  //         .then((response) => response.json())
  //         .then((responseJson) => {
  //             this.setState({
  //                 isLoading: false,
  //                 dataSource: responseJson
  //             })
  //         })
  // }
  render() {
    const {navigation} = this.props;

    return (
      <View>
        <View style={styles.container}>
          <Image
            source={require('../img/logo.jpeg')}
            style={{
              height: 100,
              width: 100,
              marginTop: '15%',
              borderRadius: 50,
            }}
          />
          <Text style={styles.judul}>Selamat Datang di Flexia</Text>
          <Text style={[styles.text, {marginBottom: 15}]}>
            Masuk untuk melanjutkan
          </Text>
          <View style={styles.kotak}>
            <Icon name="mail" size={22} style={styles.icon} />
            <TextInput
              placeholder="Masukan Email"
              style={styles.textPlaceholder}
            />
          </View>
          <View style={styles.kotak}>
            <Icon name="lock" size={22} style={styles.icon} />
            <Icon
              name={this.state.viewPassword ? 'eyeo' : 'eye'}
              size={22}
              onPress={() => {
                this.setState({viewPassword: !this.state.viewPassword});
              }}
              style={styles.icon2}
            />
            <TextInput
              placeholder="Masukan Kata Sandi"
              style={styles.textPlaceholder}
              secureTextEntry={this.state.viewPassword}
            />
          </View>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this.props.navigation.replace('MainApp');
            }}>
            <Text style={{color: White, fontSize: 16}}>Login</Text>
          </TouchableOpacity>
          <Text
            style={{
              color: Gray4,
              fontSize: 14,
              fontWeight: 'bold',
              marginBottom: 15,
            }}>
            ATAU
          </Text>
          <TouchableOpacity style={[styles.kotak, {justifyContent: 'center'}]}>
            <Image
              source={require('../img/google.png')}
              style={{
                width: 25,
                height: 25,
                marginLeft: 10,
                position: 'absolute',
                left: 0,
              }}
            />
            <View style={{alignItems: 'center'}}>
              <Text style={{color: Gray3}}>Masuk dengan Google</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.kotak, {justifyContent: 'center'}]}>
            <Icon
              name="facebook-square"
              size={22}
              style={{
                marginLeft: 12,
                position: 'absolute',
                left: 0,
                color: '#4267B2',
              }}
            />
            <View style={{alignItems: 'center'}}>
              <Text style={{color: Gray3}}>Masuk dengan Facebook</Text>
            </View>
          </TouchableOpacity>
          <Text
            style={{color: Info, fontSize: 14, marginTop: 15}}
            onPress={() => {
              this.props.navigation.navigate('Reset Password');
            }}>
            Lupa Password
          </Text>
          <Text style={{color: Gray3, fontSize: 14, marginTop: 5}}>
            Belum punya account?{' '}
            <Text
              style={{color: Info, fontSize: 14}}
              onPress={() => {
                this.props.navigation.navigate('Register');
              }}>
              Daftar
            </Text>
          </Text>
        </View>
      </View>
    );
  }
}

export default Login;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'white',
    height: '100%',
    paddingHorizontal: '4%',
  },
  judul: {
    fontWeight: 'bold',
    fontSize: 24,
    marginTop: 25,
    color: Black3,
    marginHorizontal: '7%',
    marginBottom: 7,
  },
  text: {
    fontSize: 16,
    textAlign: 'center',
    color: Gray3,
  },
  kotak: {
    flexDirection: 'row',
    width: '100%',
    borderWidth: 0.5,
    height: 45,
    borderRadius: 5,
    borderColor: Gray5,
    backgroundColor: White,
    alignItems: 'center',
    elevation: 1,
    marginBottom: 10,
  },
  icon: {
    marginHorizontal: 10,
    color: Gray3,
  },
  icon2: {
    position: 'absolute',
    right: 0,
    marginRight: 10,
    zIndex: 1,
    color: Gray3,
  },
  textPlaceholder: {
    color: Gray3,
    width: '100%',
  },
  button: {
    width: '100%',
    height: 45,
    borderRadius: 5,
    backgroundColor: Info,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
});
