import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Pressable,
} from 'react-native';
import Colors, {
  Gray1,
  Gray2,
  Gray3,
  Gray4,
  Gray5,
  Black1,
  Black2,
  Black3,
  White,
  Info,
  Success,
  Warning,
  Error,
} from '../../utils/Colors';
import Icon from 'react-native-vector-icons/AntDesign';
import HotelPreview from '../../component/HotelPreview';
import {useNavigation} from '@react-navigation/core';
import MobilRental from '../../component/MobilRental';
import {SliderBox} from 'react-native-image-slider-box';

export class Home extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {navigation} = this.props;
    return (
      <ScrollView
        style={{backgroundColor: '#e9ecef'}}
        showsVerticalScrollIndicator={false}>
        <View style={styles.topBackground}>
          <View style={styles.topComponent}>
            <View style={styles.search}>
              <Icon
                name="search1"
                size={22}
                style={{color: Info, marginLeft: 10}}
              />
              <TextInput
                style={{width: '100%'}}
                placeholder="Mau liburan kemana?"
              />
            </View>
            <Icon name="hearto" size={22} style={{color: White}} />
            <Icon name="bells" size={24} style={{color: White}} />
          </View>
        </View>
        <TouchableOpacity>
          <Image
            source={require('../img/hero.png')}
            style={{width: '100%', height: 220}}
          />
        </TouchableOpacity>
        <View style={{marginLeft: '4%'}}>
          <View>
            <Text style={styles.textKategori}>Kategori</Text>
            <View style={{flexDirection: 'row'}}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                <TouchableOpacity style={styles.iconKategori}>
                  <Image
                    source={require('../img/iconPesawat.png')}
                    style={{width: 55, height: 55}}
                  />
                  <Text style={{color: Gray3, marginTop: 2}}>Pesawat</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.iconKategori}>
                  <Image
                    source={require('../img/iconHotel.png')}
                    style={{width: 55, height: 55}}
                  />
                  <Text style={{color: Gray3, marginTop: 2}}>Hotel</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.iconKategori}>
                  <Image
                    source={require('../img/iconKereta.png')}
                    style={{width: 55, height: 55}}
                  />
                  <Text style={{color: Gray3, marginTop: 2}}>Kereta</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.iconKategori}>
                  <Image
                    source={require('../img/iconMobil.png')}
                    style={{width: 55, height: 55}}
                  />
                  <Text style={{color: Gray3, marginTop: 2}}>Mobil</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.iconKategori}>
                  <Image
                    source={require('../img/iconTiket.png')}
                    style={{width: 55, height: 55}}
                  />
                  <Text style={{color: Gray3, marginTop: 2}}>Event</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.iconKategori}>
                  <Image
                    source={require('../img/iconPesawat.png')}
                    style={{width: 55, height: 55}}
                  />
                  <Text style={{color: Gray3, marginTop: 2}}>Pesawat</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.iconKategori}>
                  <Image
                    source={require('../img/iconHotel.png')}
                    style={{width: 55, height: 55}}
                  />
                  <Text style={{color: Gray3, marginTop: 2}}>Hotel</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.iconKategori}>
                  <Image
                    source={require('../img/iconKereta.png')}
                    style={{width: 55, height: 55}}
                  />
                  <Text style={{color: Gray3, marginTop: 2}}>Kereta</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.iconKategori}>
                  <Image
                    source={require('../img/iconMobil.png')}
                    style={{width: 55, height: 55}}
                  />
                  <Text style={{color: Gray3, marginTop: 2}}>Mobil</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.iconKategori}>
                  <Image
                    source={require('../img/iconTiket.png')}
                    style={{width: 55, height: 55}}
                  />
                  <Text style={{color: Gray3, marginTop: 2}}>Event</Text>
                </TouchableOpacity>
              </ScrollView>
            </View>
          </View>
          <View style={styles.textSpecialBuatKamu}>
            <Text style={{fontSize: 15, fontWeight: 'bold', color: Black3}}>
              Special Buat Kamu
            </Text>
            <Text style={{fontSize: 15, fontWeight: 'bold', color: Info}}>
              Lihat Semua
            </Text>
          </View>
          <ScrollView
            style={{flexDirection: 'row', marginTop: 10}}
            horizontal={true}
            showsHorizontalScrollIndicator={false}>
            <Pressable
              onPress={() => {
                this.props.navigation.navigate('Hotel Deskripsi');
              }}>
              <HotelPreview
                image={require('../img/hotel/hardrockBali.png')}
                namaHotel1="Hotel"
                namaHotel2="Hard Rock"
                lokasi="Kuta, Bali"
                harga="Rp. 830.000"
                hargaAwal="Rp. 1.000.000"
                discount="17% off"
              />
            </Pressable>
            <HotelPreview
              image={require('../img/hotel/discoveryKartikaPlaza.png')}
              namaHotel1="Discovery"
              namaHotel2="Kartika Plaza"
              lokasi="Kuta, Bali"
              harga="Rp. 600.000"
              hargaAwal="Rp. 800.000"
              discount="20% off"
            />
            <HotelPreview
              image={require('../img/hotel/hardrockBali.png')}
              namaHotel1="Hotel"
              namaHotel2="Hard Rock"
              lokasi="Kuta, Bali"
              harga="Rp. 830.000"
              hargaAwal="Rp. 1.000.000"
              discount="17% off"
            />
            <HotelPreview
              image={require('../img/hotel/discoveryKartikaPlaza.png')}
              namaHotel1="Discovery"
              namaHotel2="Kartika Plaza"
              lokasi="Kuta, Bali"
              harga="Rp. 600.000"
              hargaAwal="Rp. 800.000"
              discount="20% off"
            />
          </ScrollView>
          <View style={styles.textSpecialBuatKamu}>
            <Text style={{fontSize: 15, fontWeight: 'bold', color: Black3}}>
              Roadtrip seru di Destinasimu
            </Text>
          </View>
          <Text style={{fontSize: 12, color: Gray3, marginTop: 5}}>
            Kini kamu bisa sewa mobil lepas kunci
          </Text>
          <Text style={{fontSize: 12, color: Gray3}}>
            untuk menemani liburanmu!
          </Text>
          <ScrollView
            style={{flexDirection: 'row', marginTop: 10}}
            horizontal={true}
            showsHorizontalScrollIndicator={false}>
            <MobilRental
              image={require('../img/mobil1.png')}
              namaMobil1="Avanza 2016"
              namaMobil2="lepas kunci"
              lokasi="Denpasar, Bali"
              harga="Rp. 250.000"
            />
            <MobilRental
              image={require('../img/mobil2.png')}
              namaMobil1="Innova 2017"
              namaMobil2="lepas kunci"
              lokasi="Kuta, Bali"
              harga="Rp. 450.000"
            />
            <MobilRental
              image={require('../img/mobil3.png')}
              namaMobil1="Alphard 2018"
              namaMobil2="lepas kunci"
              lokasi="Denpasar, Bali"
              harga="Rp. 950.000"
            />
            <MobilRental
              image={require('../img/mobil4.png')}
              namaMobil1="Mercy C200 2019"
              namaMobil2="lepas kunci"
              lokasi="Denpasar, Bali"
              harga="Rp. 1.550.000"
            />
          </ScrollView>
        </View>
        <View
          style={{
            paddingHorizontal: '4%',
            marginVertical: 15,
            marginBottom: 70,
          }}>
          <Image
            source={require('../img/rekomendasi.png')}
            style={{width: '100%', height: 200}}
          />
          <Text
            style={{
              fontSize: 15,
              fontWeight: 'bold',
              color: Black3,
              marginTop: 15,
            }}>
            Lepasin Penatmu
          </Text>
          <Text
            style={{
              fontSize: 12,
              color: Gray3,
              marginTop: 5,
              marginBottom: 10,
            }}>
            Tempatnya nyaman, harganya bersahabat
          </Text>
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Hotel Deskripsi');
              }}>
              <HotelPreview
                image={require('../img/hotel/hardrockBali.png')}
                namaHotel1="Hotel"
                namaHotel2="Hard Rock"
                lokasi="Kuta, Bali"
                harga="Rp. 830.000"
                hargaAwal="Rp. 1.000.000"
                discount="17% off"
              />
            </TouchableOpacity>
            <HotelPreview
              image={require('../img/hotel/discoveryKartikaPlaza.png')}
              namaHotel1="Discovery"
              namaHotel2="Kartika Plaza"
              lokasi="Kuta, Bali"
              harga="Rp. 600.000"
              hargaAwal="Rp. 800.000"
              discount="20% off"
            />
          </View>
          <View style={{flexDirection: 'row'}}>
            <HotelPreview
              image={require('../img/hotel/discoveryKartikaPlaza.png')}
              namaHotel1="Discovery"
              namaHotel2="Kartika Plaza"
              lokasi="Kuta, Bali"
              harga="Rp. 600.000"
              hargaAwal="Rp. 800.000"
              discount="20% off"
            />
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Hotel Deskripsi');
              }}>
              <HotelPreview
                image={require('../img/hotel/hardrockBali.png')}
                namaHotel1="Hotel"
                namaHotel2="Hard Rock"
                lokasi="Kuta, Bali"
                harga="Rp. 830.000"
                hargaAwal="Rp. 1.000.000"
                discount="17% off"
              />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default Home;

const styles = StyleSheet.create({
  topBackground: {
    paddingTop: '5%',
    paddingHorizontal: '4%',
    backgroundColor: Info,
  },
  topComponent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 12,
  },
  search: {
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: White,
    height: 45,
    width: '73%',
    borderRadius: 5,
    backgroundColor: White,
    alignItems: 'center',
  },
  textKategori: {
    color: Black2,
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10,
    marginTop: 15,
  },
  iconKategori: {
    flexDirection: 'column',
    alignItems: 'center',
    marginHorizontal: 7,
  },
  textSpecialBuatKamu: {
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: '4%',
  },
});
