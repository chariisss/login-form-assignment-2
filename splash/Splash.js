import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {useNavigation} from '@react-navigation/core';
import {StackActions} from '@react-navigation/native';

export class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.dispatch(StackActions.replace('Login'));
    }, 2000);
  }
  render() {
    return (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#00CCFF',
          height: '100%',
        }}>
        <Image
          source={require('../img/logo.jpeg')}
          style={{width: 130, height: 130}}
        />
      </View>
    );
  }
}

export default Splash;
